#include <fstream>
#include <stdexcept>

#include "pairings.h"
#include "muse.h"

namespace muse{
    // XXX there will be more parameters in the future
    pairings::G2 paramGen(){
        return pairings::gen_random_g2();
    }
    namespace writer{
        pairings::Zr keyGen(){
            return pairings::generate_random_zr_elt();
        }
        std::vector<pairings::GT> encrypt( std::string source_filename, pairings::Zr writer_secret_key, pairings::G2 gen_g2 ){
            std::vector<pairings::GT> result;
            std::ifstream input_stream(source_filename);
            for( std::string keyword; getline( input_stream, keyword ); ){
                result.push_back(pairings::pair(
                        pairings::hash_to_G1(keyword),
                        pairings::pow(gen_g2, writer_secret_key)
                    )
                );
            }
            return result;
        }
        pairings::G2 delegate( pairings::G2 reader_pub_key, pairings::Zr writer_secret_key ){
            return pairings::pow(reader_pub_key, writer_secret_key);
        }
    }
    namespace reader{
        std::tuple<pairings::Zr,pairings::G2> keyGen(pairings::G2 gen_g2){
            pairings::Zr sec_key = pairings::generate_random_zr_elt();
            // XXX should be inv(reader_secret_key),
            // but with our fake pairings it is equivalent
            pairings::G2 pub_key = pairings::pow(gen_g2, sec_key);
            
            return make_tuple(sec_key, pub_key);
        }

        std::tuple<pairings::Zr,pairings::G1> trapdoor( std::string keyword, pairings::Zr reader_secret_key ){
            pairings::Zr xi = pairings::generate_random_zr_elt();
            pairings::G1 t = pairings::pow(
                        pairings::pow(
                            pairings::hash_to_G1(keyword),
                            xi
                        ),
                        reader_secret_key
                   );
            // https://stackoverflow.com/a/35098255/3025740
            return std::make_tuple(xi, t);
        }
    }
    namespace qm{
        pairings::GT transform( pairings::G1 trapdoor, pairings::G2 auth){
            return pairings::pair(trapdoor, auth);
            // TODO use BFs and OT etc...
        }
    }
    namespace dh{
        std::vector<pairings::GT> prepare( std::vector<pairings::GT> enc_index, pairings::Zr xi){
            std::vector<pairings::GT> result;
            for( auto enc_kw : enc_index ){
                result.push_back( pairings::pow(enc_kw, xi) );
            }
            return result;
        }
    }
}


void write_to_file( std::string destination_filename, std::vector<pairings::GT> enc_index){
    std::ofstream output_stream(destination_filename);
    for( auto enc_kw : enc_index ){
        output_stream << pairings::serialize(enc_kw) << std::endl;
    }
    output_stream.close();
}

void write_to_file( std::string destination_filename, pairings::G1 trapdoor){
    std::ofstream output_stream(destination_filename);
    output_stream << pairings::serialize(trapdoor) << std::endl;
    output_stream.close();
}

pairings::G2 read_elt_from_file(std::string filename){
    std::ifstream input_stream(filename);
    std::string str;
    if (input_stream >> str){
        return pairings::deserialize(str);
    } else {
        throw "could not read from " + filename;
    }
}

std::vector<pairings::GT> read_enc_index_from_file( std::string filename ){
    std::vector<pairings::GT> result;
    std::ifstream input_stream(filename);
    for( std::string line; getline( input_stream, line ); ){
        result.push_back( pairings::deserialize(line) );
    }
    return result;
}

