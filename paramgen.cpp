#include "muse.h"

int main(int argc, const char *argv[])
{
    if (argc != 2){
        std::cout << "usage: paramgen <dest_file>" << std::endl;
    }

    std::string dest_filename = argv[1];

    pairings::G2 gen_g2 = muse::paramGen();

    write_to_file(dest_filename, gen_g2);

    return 0;
}
