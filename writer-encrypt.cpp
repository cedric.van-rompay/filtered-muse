#include "muse.h"
#include <iostream>

int main(int argc, const char *argv[])
{
    if (argc != 4){
        std::cout << "usage: writer-encrypt <index file> <writer sec key> <enc index dest file>" << std::endl;
        return 1;
    }
    std::string index_filename = argv[1];
    std::string writer_sec_key_filename = argv[2];
    std::string enc_index_filename = argv[3];

    pairings::Zr writer_sec_key = read_elt_from_file(writer_sec_key_filename);
    pairings::G2 gen_g2 = read_elt_from_file("dump/gen_g2");

    std::vector<pairings::GT> enc_index = muse::writer::encrypt( index_filename, writer_sec_key, gen_g2);
    
    write_to_file( enc_index_filename, enc_index );

    return 0;
}
