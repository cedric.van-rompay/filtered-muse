# Required by XPIR
# ( https://github.com/XPIR-team/XPIR#installation )
# FIXME We take latest version from debian instead of version recommended by XPIR
# is this doesn't work it would be better to use XPIR/helper_script.sh
# (slower)
sudo apt-get install -y cmake
sudo apt-get install -y libgmp-dev
sudo apt-get install -y libmpfr-dev
sudo apt-get install -y libboost-all-dev

# Required by the python scripts (utilities and test suite)
pip3 install --user click
pip3 install --user PyYAML

# Required for the software
sudo apt-get install libssl-dev
