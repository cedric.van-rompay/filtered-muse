#include "muse.h"
#include <iostream>

int main(int argc, const char *argv[])
{
    if (argc != 5){
        std::cout << "usage: " << argv[0] << " <keyword> <reader sec key> <trapdoor dest file> <rand factor dest file>" << std::endl;
        return 1;
    }
    std::string keyword = argv[1];
    std::string reader_sec_key_filename = argv[2];
    std::string trapdoor_filename = argv[3];
    std::string rand_factor_filename = argv[4];

    pairings::Zr reader_sec_key = read_elt_from_file(reader_sec_key_filename);

    pairings::Zr rand_factor;
    pairings::G1 trapdoor;
    std::tie(rand_factor, trapdoor) = muse::reader::trapdoor( keyword, reader_sec_key );

    write_to_file( trapdoor_filename, trapdoor );
    write_to_file( rand_factor_filename, rand_factor );

    return 0;
}
