#include <iostream>
#include <fstream>

#include "muse.h"
#include "ZGBF.h"

int main(int argc, const char *argv[])
{
    if (argc != 4){
        std::cout << "usage: " << argv[0] << " <enc index file> <rand factor file> <prep enc index dest file>" << std::endl;
        return 1;
    }
    std::string enc_index_filename = argv[1];
    std::string rand_factor_filename = argv[2];
    std::string prepared_enc_index_filename = argv[3];

    std::vector<pairings::GT> enc_index = read_enc_index_from_file(enc_index_filename);
    pairings::Zr rand_factor = read_elt_from_file(rand_factor_filename);

    std::vector<pairings::GT> prepared_enc_index = muse::dh::prepare( enc_index, rand_factor );

	ZGBF zgbf(1024, 8);
    for (auto elt: prepared_enc_index){
        std::vector<uint8_t> vectorized_elt (elt.begin(), elt.end());
        zgbf.add(vectorized_elt);
    }
    zgbf.finalize();

    // TODO make optional dump of pre-zgbf prepared index
    // write_to_file( prepared_enc_index_filename, prepared_enc_index );
    
    std::vector<uint64_t> components = zgbf.get_components();
	std::ofstream out_file_stream (prepared_enc_index_filename, std::ofstream::binary);
    // FIXME C-style casting ?
	out_file_stream.write ( (const char*) components.data(), components.size() * sizeof(uint64_t)	);
	out_file_stream.close();

    return 0;
}
