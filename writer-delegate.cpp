#include "muse.h"
#include <iostream>

int main(int argc, const char *argv[])
{
    if (argc != 4){
        std::cout << "usage: writer-delegate <reader pub key file> <writer sec key file> <auth dest file>" << std::endl;
        return 1;
    }
    std::string reader_pub_key_filename = argv[1];
    std::string writer_sec_key_filename = argv[2];
    std::string auth_filename = argv[3];

    pairings::G2 reader_pub_key = read_elt_from_file(reader_pub_key_filename);
    pairings::Zr writer_sec_key = read_elt_from_file(writer_sec_key_filename);

    pairings::G2 auth = muse::writer::delegate( reader_pub_key, writer_sec_key );

    write_to_file( auth_filename, auth );

    return 0;
}
