#include <array>
#include <vector>
#include <algorithm>
#include <iostream>
#include <sstream>
#include <iomanip>

#include <openssl/rand.h>
#include <openssl/sha.h>

namespace pairings{
    using Zr = std::array<uint8_t, 8>;
    using G1 = std::array<uint8_t, 8>;
    using G2 = std::array<uint8_t, 8>;
    using GT = std::array<uint8_t, 8>;

    Zr generate_random_zr_elt();

    std::string serialize( Zr x );
    Zr deserialize( std::string str );

    G1 hash_to_G1(const std::string str);

    G2 gen_random_g2();

    G1 pow( G1, Zr );
    GT pair( G1, G2 );
}

std::string operator<<(std::ofstream os, pairings::GT x);
