# Requirements

Install requirements by running:
```
bash setup.sh
```

# Build

```
make
```
# Test

Example of keywords:

- google.com (in both indexes)
- wikipedia.org (in bob's)
- yahoo.com (in none)

```
bash tests/test.sh [some keyword]
```
