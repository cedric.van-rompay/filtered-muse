// Fake for now, uses XOR instead (correct but insecure)

#include "pairings.h"

namespace pairings{

    Zr generate_random_zr_elt(){
        Zr result;
        RAND_bytes( result.data(), result.size() );
        return result;
    }

    // all type are synonnyms here, so function is defined once
    std::string serialize( Zr x ){
        std::ostringstream ss;
        ss << std::hex << std::uppercase << std::setfill( '0' );
        for( auto c : x ) {
            // https://stackoverflow.com/a/19562163/3025740
            ss << std::setw( 2 ) << unsigned(c);
        }
        return ss.str();
    }

    Zr deserialize( std::string str ){
	  Zr result;

	  for (unsigned int i = 0; i < str.length(); i += 2) {
		std::string byteString = str.substr(i, 2);
		char byte = (char) strtol(byteString.c_str(), NULL, 16);
		result[i] = byte;
	  }

	  return result;
    }

    G1 hash_to_G1(const std::string str)
    {
        unsigned char hash[SHA256_DIGEST_LENGTH];
        SHA256_CTX sha256;
        SHA256_Init(&sha256);
        SHA256_Update(&sha256, str.c_str(), str.size());
        SHA256_Final(hash, &sha256);

        G1 result;
        std::copy(hash, hash + 8, result.begin());
        return result;
    }

    G2 gen_random_g2(){
        G2 gen_g2;
        gen_g2.fill(0);
        return gen_g2;
    }

    std::array<uint8_t, 8> pow( std::array<uint8_t, 8> x, std::array<uint8_t, 8> y){
        std::array<uint8_t, 8> result;
        // https://stackoverflow.com/a/13123280/3025740
        std::transform( x.begin(), x.end(), y.begin(), result.begin(), std::bit_xor<uint8_t>() );
        return result;
    }

    // https://stackoverflow.com/a/18919937/3025740
    GT pair( G1 x, G2 y){
        std::array<uint8_t, 8> result;
        // https://stackoverflow.com/a/13123280/3025740
        std::transform( x.begin(), x.end(), y.begin(), result.begin(), std::bit_xor<uint8_t>() );
        return result;
    }

}

// https://msdn.microsoft.com/en-us/library/1z2f6c2k.aspx
std::ostream& operator<<(std::ostream& os, const pairings::GT x){
    std::ostringstream ss;
    ss << std::hex << std::uppercase << std::setfill( '0' );
    for( auto c : x ) {
        ss << std::setw( 2 ) << c;
    }
    os << ss.str();
    return os;
}
