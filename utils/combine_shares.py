#!/usr/bin/python3
# coding: utf-8
import binascii
import sys
import random
import os

import yaml
import click

PIR_CLIENT_DIRECTORY = "XPIR/_build/apps/client/"

def hexlify(x):
    return binascii.hexlify(x).decode().upper()

def xor(*args):
    result = bytes(len(args[0]))
    for each in args:
        result = bytes( x ^ y for (x,y) in zip(result, each) )
    return result

# quick test
a,b,c = (bytes([random.getrandbits(8) for _ in range(8)]) for _ in range(3))
assert xor( bytes([0xff, 0xff]), bytes([0xf0, 0xf0]) ) == bytes([0x0f, 0x0f])
assert xor(a,b,c) == xor(a, xor(b,c)) == xor( xor(a,b), c)
assert not any(xor(a,a))

@click.command()
@click.argument('index_id')
@click.argument('response_destination_file')
@click.option('--debug/--no-debug', default=False)
def main(index_id, response_destination_file, debug):
    result = xor(*[
        open(PIR_CLIENT_DIRECTORY + "/reception/" + share_filename, 'rb').read()
        for share_filename in os.listdir(PIR_CLIENT_DIRECTORY + "/reception/")
    ])

    if all([ byte==0 for byte in result]) and not debug:
        with open(response_destination_file, 'a') as f:
            f.write(index_id + '\n')

    if debug:
        print([
        str(open(PIR_CLIENT_DIRECTORY + "/reception/" + share_filename, 'rb').read())
        for share_filename in os.listdir(PIR_CLIENT_DIRECTORY + "/reception/")
    ])
        print(result)
    

if __name__ == '__main__' and '__file__' in globals():
	main()
    
