#!/usr/bin/python3
# coding: utf-8
import subprocess
from pprint import pprint
import logging

import yaml
import click
import shutil
import os

PIR_CLIENT_DIRECTORY = "XPIR/_build/apps/client/"
RECEPTION_DIR = PIR_CLIENT_DIRECTORY + "/reception/"

def hexlify(x):
    return binascii.hexlify(x).decode().upper()

def xor(*args):
    result = bytes(len(args[0]))
    for each in args:
        result = bytes( x ^ y for (x,y) in zip(result, each) )
    return result

def clean_reception_dir():
    shutil.rmtree(RECEPTION_DIR)
    os.mkdir(RECEPTION_DIR)

def transform(trapdoor, auth, destination):
    subprocess.run(["bin/qm-transform", trapdoor, auth, destination],  check=True)

def load_yaml_from_file(filename):
    with open(filename) as f:
        return yaml.load(f)

def pir_retrieve(position, port):
    p = subprocess.Popen([
                "./pir_client",
                "--choice", str(position),
                "--port", str(port),
            ],
            cwd=PIR_CLIENT_DIRECTORY,
            stdout=subprocess.DEVNULL,
    )
    p.wait()
    if p.returncode != 0:
        raise Exception('./pir_client exited with return code != 0')

def load_retrieved_share(share_filename):
    with open(RECEPTION_DIR + str(share_filename), 'rb') as f:
        return f.read()

def is_all_zeroes(share):
    return True if all([ byte==0 for byte in share]) else False

@click.command()
@click.argument('trapdoor')
@click.argument('auth')
@click.argument('transformed_trapdoor_destination')
@click.argument('index_id')
@click.argument('response_destination_file')
@click.argument('port')
def main(trapdoor, auth, transformed_trapdoor_destination, index_id, response_destination_file, port):
    clean_reception_dir()

    transform(trapdoor, auth, transformed_trapdoor_destination)

    positions = load_yaml_from_file(transformed_trapdoor_destination)
        
    result = bytes(8)
    for position in positions:
        pir_retrieve(position, port)
        retrieved_share = load_retrieved_share(position)
        result = xor(result, retrieved_share)

    if all([ byte==0 for byte in result]):
        with open(response_destination_file, 'a') as f:
            f.write(index_id + '\n')

if __name__ == '__main__' and '__file__' in locals():
    main()

