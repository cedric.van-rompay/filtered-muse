#!/usr/bin/python3
# coding: utf-8
import subprocess
import shutil
import os
import hashlib
import binascii

import click

SERVER_DIRECTORY = "XPIR/_build/apps/server/"

def get_tmp_dir_name(file_to_serve):
    digest = hashlib.md5(file_to_serve.encode()).digest()
    return binascii.hexlify(digest).decode()[:4]

def prepare_tmp_dir(path_to_dir, db_file):
    shutil.rmtree(path_to_dir, ignore_errors=True)
    os.makedirs(path_to_dir)
    shutil.copytree('XPIR/exp', path_to_dir +'/exp')
    os.mkdir(path_to_dir + '/reception')
    os.mkdir(path_to_dir + '/db')
    shutil.copy(db_file, path_to_dir + '/db/')


@click.command()
@click.argument('file_to_serve')
@click.argument('port', default=3000)
def main(file_to_serve, port):

    tmp_dirname = '/tmp/muse-test/' + get_tmp_dir_name(file_to_serve)
    prepare_tmp_dir(tmp_dirname, file_to_serve)

    p = subprocess.Popen([
            os.path.abspath("XPIR/_build/apps/server/pir_server"),
            "--split_file","1024",
            "--port", str(port),
            ],
            cwd=tmp_dirname,
        )

    p.wait()

if __name__ == '__main__':
    main()
