#!/usr/bin/python3
# coding: utf-8
import binascii
import sys
import random

import yaml
import click

class ZGBF:
    def __init__(self, data, component_byte_length):
        self.component_byte_length = component_byte_length

        shares_number = len(data)//component_byte_length
        self.shares = tuple(
                data[component_byte_length*i : component_byte_length*(i+1)]
                for i in range(shares_number)
        )

    @classmethod
    def load(cls, input_file, component_byte_length):
        return ZGBF( input_file.read(), component_byte_length) 

def hexlify(x):
    return binascii.hexlify(x).decode().upper()

def xor(*args):
    result = bytes(len(args[0]))
    for each in args:
        result = bytes( x ^ y for (x,y) in zip(result, each) )
    return result

# quick test
a,b,c = (bytes([random.getrandbits(8) for _ in range(8)]) for _ in range(3))
assert xor( bytes([0xff, 0xff]), bytes([0xf0, 0xf0]) ) == bytes([0x0f, 0x0f])
assert xor(a,b,c) == xor(a, xor(b,c)) == xor( xor(a,b), c)
assert not any(xor(a,a))

@click.command()
@click.argument('path_to_zgbf_file')
@click.argument('path_to_positions_file')
def main(path_to_zgbf_file, path_to_positions_file):
    with open(path_to_zgbf_file, 'rb') as f:
        zgbf = ZGBF.load(f, component_byte_length = 8)

    with open(path_to_positions_file) as f:
        positions = yaml.load(f)

    for position in positions:
        print('{:<4}: {}'.format(position, hexlify(zgbf.shares[position])))

    shares_sum = xor(*[zgbf.shares[position] for position in positions])
    print('XOR : %s' % hexlify(shares_sum))

    result = "PRESENT" if not any(shares_sum) else "ABSENT"
    print("RESULT:", result)

if __name__ == '__main__' and '__file__' in globals():
	main()
