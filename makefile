CFLAGS = -std=c++11
SOURCES = muse.cpp pairings.cpp MurmurHash3.cpp ZGBF.cpp

default: dirs xpir \
	bin/writer-keygen \
	bin/paramgen \
	bin/reader-keygen \
	bin/writer-delegate \
	bin/writer-encrypt \
	bin/reader-trapdoor \
	bin/dh-prepare \
	bin/qm-transform \
	bin/dh-serve \
	bin/qm-transform-and-retrieve \
	bin/combine_shares

xpir: XPIR/_build
	(cd XPIR/_build ; make)

.PHONY: xpir

XPIR/_build:
	mkdir -p XPIR/_build
	(cd XPIR/_build; cmake .. -DCMAKE_BUILD_TYPE=Release)

bin/writer-keygen: writer-keygen.cpp $(SOURCES)
	g++ -o $@ $^ $(CFLAGS) -lcrypto

bin/paramgen: paramgen.cpp $(SOURCES)
	g++ -o $@ $^ $(CFLAGS) -lcrypto

bin/reader-keygen: reader-keygen.cpp $(SOURCES)
	g++ -o $@ $^ $(CFLAGS) -lcrypto

bin/writer-delegate: writer-delegate.cpp $(SOURCES)
	g++ -o $@ $^ $(CFLAGS) -lcrypto

bin/writer-encrypt: writer-encrypt.cpp $(SOURCES)
	g++ -o $@ $^ $(CFLAGS) -lcrypto

bin/reader-trapdoor: reader-trapdoor.cpp $(SOURCES)
	g++ -o $@ $^ $(CFLAGS) -lcrypto

bin/dh-prepare: dh-prepare.cpp $(SOURCES)
	g++ -o $@ $^ $(CFLAGS) -lcrypto

bin/qm-transform: qm-transform.cpp $(SOURCES)
	g++ -o $@ $^ $(CFLAGS) -lcrypto

bin/dh-serve: utils/dh-serve.py
	cp utils/dh-serve.py bin/dh-serve

bin/qm-transform-and-retrieve: utils/qm-transform-and-retrieve.py
	cp utils/qm-transform-and-retrieve.py bin/qm-transform-and-retrieve

bin/combine_shares: utils/combine_shares.py
	cp utils/combine_shares.py bin/combine_shares


bin/pairings_test: tests/pairings_test.cpp pairings.cpp
	g++ -o $@ $^ $(CFLAGS) -lcrypto

bin/zgbf_test: tests/zgbf_test.cpp ZGBF.cpp MurmurHash3.cpp
	g++ -o $@ $^ $(CFLAGS) -lcrypto

dirs:
	@mkdir -p bin dump

.PHONY: default dirs
