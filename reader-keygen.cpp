#include "muse.h"
#include <iostream>

int main(int argc, const char *argv[])
{
    if (argc != 3){
        std::cout << "usage: reader-keygen <reader sec key dest file> <reader pub key dest file>" << std::endl;
        return 1;
    }
    std::string sec_key_filename = argv[1];
    std::string pub_key_filename = argv[2];

    pairings::G2 gen_g2 = read_elt_from_file("dump/gen_g2");

    pairings::Zr sec_key;
    pairings::G2 pub_key;
    std::tie(sec_key, pub_key) = muse::reader::keyGen( gen_g2 );

    write_to_file( sec_key_filename, sec_key );
    write_to_file( pub_key_filename, pub_key );

    return 0;
}
