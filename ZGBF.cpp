#include "ZGBF.h"

ZGBF::ZGBF(uint64_t size, uint8_t numHashes)
      : m_components(size),
        m_numHashes(numHashes) {}

std::vector<uint64_t> ZGBF::map( const std::vector<uint8_t> element ) const {
	/*
	* "Double hashing" technique:
	* obtaining many hash functions with only two calls to hash functions
	* See "Less hashing, same performance: Building a better Bloom filter" by Kirsch and Mitzenmacher:
	* http://dx.doi.org/10.1002/rsa.20208

	Actually here we make a single call to MurmurHash3_x64_128 because it outputs 128 bits
	and we need 2 times 64 bits
	*/

	std::array<uint64_t, 2> hashValues;
	MurmurHash3_x64_128(element.data(), element.size(), 0, hashValues.data());

	std::vector<uint64_t> positions(m_numHashes);
	for (int n = 0; n < m_numHashes; n++) {
		positions[n] = (hashValues[0] + n * hashValues[1]) % m_components.size();
	}

	return positions;
}

/*
	See Algorithm 1, lines 6 to 20
	In "When private set intersection meets big data: an efficient and scalable protocol"
	by Dong, Chen and When, published in ACM CCS 2013
*/
void ZGBF::add( const std::vector<uint8_t> element ) {
	std::vector<uint64_t> positions = map( element );

	// Initialization
	bool emptySlot_exists = false;
	uint64_t emptySlot_address = 0;

	// The value the shares will sum to
	// Fixed value equal to zero in ZGBF
	// Differs from GBF
	uint64_t finalShare = 0;

  for (int n = 0; n < m_numHashes; n++) {
			// FIXME it is possible (though unlikely) that a non-empty component is set to zero.
			if (m_components[positions[n]] == 0){
				if ( !emptySlot_exists ){
					// reserve location for final share
					emptySlot_exists = true;
					emptySlot_address = positions[n];
				}else{
					// fill with fresh secret share
					uint64_t share = 0;
					RAND_bytes( (unsigned char *)&share, sizeof(uint64_t) );
					memcpy( &m_components[positions[n]], &share, sizeof(uint64_t));

					// update final share for correctness of XOR secret shares
					finalShare = finalShare ^ share;
				}
			} else {
				// update final share for correctness of XOR secret shares
				// FIXME Can factorize with previous instruction
				finalShare = finalShare ^ m_components[positions[n]];
			}
  }
	m_components[emptySlot_address] = finalShare;
}

/*
	See Algorithm 1, lines 22 to 26
	In "When private set intersection meets big data: an efficient and scalable protocol"
	by Dong, Chen and When, published in ACM CCS 2013
*/
void ZGBF::finalize(){
	for (int n = 0; n < m_components.size(); n++){
		if (m_components[n] == 0){
			// Fill component with random value
			uint64_t share = 0;
			RAND_bytes( (unsigned char *)&share, sizeof(uint64_t) );
			memcpy( &m_components[n], &share, sizeof(uint64_t));
		}
	}

}

std::vector<uint64_t> ZGBF::extract_shares( std::vector<uint64_t> positions ) const{
	std::vector<uint64_t> shares;
	for ( auto position: positions ){
		shares.push_back( m_components[position] );
	}
	return shares;
}

bool ZGBF::check( std::vector<uint64_t> shares ) const{
	uint64_t accumulator = 0;
  for ( auto share: shares ) {
		accumulator = accumulator ^ share;
  }

	if (accumulator == 0){
		return true;
	} else {
		return false;
	}
}

bool ZGBF::query( const std::vector<uint8_t> element ) const {
	std::vector<uint64_t> positions = map( element );

	std::vector<uint64_t> shares = extract_shares( positions );

	return check( shares );
}

std::vector<uint64_t> ZGBF::get_components(){
	return m_components;
}
