#include <vector>

#include "pairings.h"

namespace muse{
    pairings::G2 paramGen();
    namespace writer{
        pairings::Zr keyGen();
        std::vector<pairings::GT> encrypt( std::string source_filename, pairings::Zr writer_secret_key, pairings::G2 gen_g2 );
        pairings::G2 delegate( pairings::G2 reader_pub_key, pairings::Zr writer_secret_key );
    }
    namespace reader{
        std::tuple<pairings::Zr,pairings::G2> keyGen(pairings::G2 gen_g2);
        std::tuple<pairings::Zr,pairings::G1> trapdoor( std::string keyword, pairings::Zr reader_secret_key );
    }
    namespace qm{
        pairings::GT transform( pairings::G1 trapdoor, pairings::G2 auth);
    }
    namespace dh{
        std::vector<pairings::GT> prepare( std::vector<pairings::GT> enc_index, pairings::Zr xi);
    }
}

void write_to_file( std::string destination_filename, std::vector<pairings::GT> enc_index);
void write_to_file( std::string destination_filename, pairings::G1 trapdoor);
pairings::G2 read_elt_from_file(std::string filename);
std::vector<pairings::GT> read_enc_index_from_file( std::string filename );
