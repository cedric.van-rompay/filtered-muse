#include <iostream>
#include <fstream>

#include "muse.h"
#include "ZGBF.h"

int main(int argc, const char *argv[])
{
    if (argc != 4){
        std::cout << "usage: " << argv[0] << " <trapdoor> <auth> <transformed trapdoor dest file>" << std::endl;
        return 1;
    }
    std::string trapdoor_filename = argv[1];
    std::string auth_filename = argv[2];
    std::string transformed_trapdoor_filename = argv[3];

    pairings::G1 trapdoor = read_elt_from_file(trapdoor_filename);
    pairings::G2 auth = read_elt_from_file(auth_filename);

    pairings::GT transformed_trapdoor = muse::qm::transform( trapdoor, auth );

    ZGBF zgbf(1024, 8);

	std::ofstream positionsFile (transformed_trapdoor_filename);
    std::vector<uint8_t> vectorized_transformed_trapdoor (transformed_trapdoor.begin(), transformed_trapdoor.end());
    std::vector<uint64_t> positions = zgbf.map( vectorized_transformed_trapdoor );
    positionsFile << "[";
    for (auto position: positions){
        positionsFile << position;
        positionsFile << ',';
    }
    positionsFile << "]" << std::endl;
	positionsFile.close();

    return 0;
}
