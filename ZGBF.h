#include <vector>
#include <array>
#include <cstring>

#include "MurmurHash3.h"
#include <openssl/rand.h>

struct ZGBF {
    ZGBF(uint64_t size, uint8_t numHashes);
    
    std::vector<uint64_t> map( const std::vector<uint8_t> element ) const;
    void add( const std::vector<uint8_t> element );
    std::vector<uint64_t> extract_shares( std::vector<uint64_t> positions ) const;
    bool check( std::vector<uint64_t> shares ) const;
    bool query( const std::vector<uint8_t> element ) const;
    std::vector<uint64_t> get_components();
    void finalize();

    private:
        uint8_t m_numHashes;
        std::vector<uint64_t> m_components;
};
