#include "muse.h"
#include <iostream>

int main(int argc, const char *argv[])
{
    if (argc != 2){
        std::cout << "usage: " << argv[0] << " dest_file" << std::endl;
        return 1;
    }
    // TODO define new types for muse
    pairings::Zr sec_key = muse::writer::keyGen();

    std::string destination_filename = argv[1];
    write_to_file( destination_filename, sec_key );

    return 0;
}
