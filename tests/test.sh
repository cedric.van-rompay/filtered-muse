shopt -s extglob  #Enables extglob

rm -f dump/*

bin/paramgen "dump/gen_g2" &&\

bin/writer-keygen "dump/alice.writer.secret.key" &&\
bin/writer-keygen "dump/bob.writer.secret.key" &&\

bin/reader-keygen "dump/charlie.reader.secret.key" "dump/charlie.reader.public.key" &&\

bin/writer-delegate "dump/charlie.reader.public.key" "dump/alice.writer.secret.key" "dump/charlie.alice.auth" &&\
bin/writer-delegate "dump/charlie.reader.public.key" "dump/bob.writer.secret.key" "dump/charlie.bob.auth" &&\

bin/writer-encrypt "data/alice.index" "dump/alice.writer.secret.key" "dump/alice.index.encrypted" &&\
bin/writer-encrypt "data/bob.index" "dump/bob.writer.secret.key" "dump/bob.index.encrypted" &&\

QUERIED_KEYWORD=$1

echo "Creating query for keyword '$QUERIED_KEYWORD'"

bin/reader-trapdoor $QUERIED_KEYWORD "dump/charlie.reader.secret.key" "dump/charlie.google.com.trapdoor" "dump/charlie.google.com.rand_factor" &&\

bin/dh-prepare "dump/alice.index.encrypted" "dump/charlie.google.com.rand_factor" "dump/alice.index.encrypted.prepared.bin" &&\
bin/dh-prepare "dump/bob.index.encrypted" "dump/charlie.google.com.rand_factor" "dump/bob.index.encrypted.prepared.bin" &&\

bin/qm-transform "dump/charlie.google.com.trapdoor" "dump/charlie.alice.auth" "dump/charlie.google.com.trapdoor.alice.transformed" &&\
bin/qm-transform "dump/charlie.google.com.trapdoor" "dump/charlie.bob.auth" "dump/charlie.google.com.trapdoor.bob.transformed" &&\


bin/dh-serve dump/alice.index.encrypted.prepared.bin 3001 &> /tmp/1.pir_server.log &
PIR_SERVER_PID=$!

sleep 0.5

bin/qm-transform-and-retrieve "dump/charlie.google.com.trapdoor" "dump/charlie.alice.auth" "dump/charlie.google.com.trapdoor.alice.transformed" "alice.index" "dump/charlie.google.com.response" 3001 # &> /tmp/1.pir_client.log 

kill $PIR_SERVER_PID


sleep 0.5


bin/dh-serve dump/bob.index.encrypted.prepared.bin 3002 &> /tmp/2.pir_server.log &
PIR_SERVER_PID=$!

sleep 0.5

bin/qm-transform-and-retrieve "dump/charlie.google.com.trapdoor" "dump/charlie.bob.auth" "dump/charlie.google.com.trapdoor.bob.transformed" "bob.index" "dump/charlie.google.com.response" 3002 # &> /tmp/2.pir_client.log 

kill $PIR_SERVER_PID


echo Done.
echo Result for keyword $QUERIED_KEYWORD:
cat "dump/charlie.google.com.response"
