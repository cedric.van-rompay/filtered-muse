#include <iostream>
#include "../pairings.h"

int main(int argc, const char *argv[])
{
    std::cout << "# Testing bilinearity" << std::endl;

    pairings::G2 gen_g2 = pairings::gen_random_g2();

    pairings::Zr alpha = pairings::generate_random_zr_elt();
    pairings::Zr beta = pairings::generate_random_zr_elt();

    pairings::G1 x = pairings::hash_to_G1("test string");

    pairings::GT result_1 = pairings::pair(pairings::pow(x, alpha), pairings::pow(gen_g2, beta));
    pairings::GT result_2 = pairings::pow( pairings::pow( pairings::pair(x, gen_g2), alpha ), beta );

    if (result_1 == result_2){
        std::cout << "[OK]" << std::endl;
    } else {
        std::cout << "[FAILED]" << std::endl;
    }

    return 0;
}
